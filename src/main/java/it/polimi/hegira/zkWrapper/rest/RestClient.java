/**
 * Copyright 2014 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.zkWrapper.rest;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

/**
 * @author Fabio Arcidiacono.
 */
public class RestClient {

    private static final String CHARSET = "UTF-8";
    private String basePath;

    public RestClient(String basePath) {
        this.basePath = basePath;
    }

    /**
     * Returns a range of maximum 100 (minimum 2) unique ids to the caller for a given table.
     *
     * @param tableName The name of the table.
     * @param offset    the number of ids to be assigned (less than 101)
     *
     * @return An array containing the first and the last ids in the assigned range.
     *
     * @throws Exception ZK errors, interruptions, etc.
     */
    public int[] assignSeqNrRange(String tableName, int offset) throws Exception {
        URL url = assignSeqNrRangeURL(tableName, offset);
        JSONObject response = getJsonObject(url);
        return new int[]{response.getInt("lowExtreme"), response.getInt("highExtreme")};
    }

    /**
     * Returns a unique id to the caller for a given table.
     *
     * @param tableName The name of the table.
     *
     * @return The unique id
     *
     * @throws Exception ZK errors, interruptions, etc.
     */
    public int assignSeqNr(String tableName) throws Exception {
        URL url = assignSeqNrURL(tableName);
        JSONObject response = getJsonObject(url);
        return response.getInt("id");
    }

    /**
     * Returns the status of Hegira, i.e., if it has been configured to synchronize data.
     *
     * @return <b>true</b> if it is synchronizing, <b>false</b> otherwise
     *
     * @throws Exception ZK errors, interruptions, etc.
     */
    public boolean isSynchronizing() throws Exception {
        URL url = isSynchronizingURL();
        JSONObject response = getJsonObject(url);
        return response.getBoolean("synch");
    }

    private URL assignSeqNrRangeURL(String table, int offset) throws IOException {
        String params = String.format("getRange?tableName=%s&offset=%s", URLEncoder.encode(table, CHARSET), URLEncoder.encode(String.valueOf(offset), CHARSET));
        return new URL(basePath + params);
    }

    private URL assignSeqNrURL(String table) throws IOException {
        String params = String.format("getId?tableName=%s", URLEncoder.encode(table, CHARSET));
        return new URL(basePath + params);
    }

    private URL isSynchronizingURL() throws IOException {
        return new URL(basePath + "isSynchronizing");
    }

    private JSONObject getJsonObject(URL url) throws IOException {
        JSONObject response = responseAsJSON(url);
        if (!isSuccess(response)) {
            String msg = buildErrorMessage(response);
            throw new IOException(msg);
        }
        return response;
    }

    private JSONObject responseAsJSON(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        return new JSONObject(convertStreamToString(connection.getInputStream()));
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    private boolean isSuccess(JSONObject response) {
        return "OK".equalsIgnoreCase(response.getJSONObject("requestStatus").getString("status"));
    }

    private String buildErrorMessage(JSONObject response) {
        JSONObject requestStatus = response.getJSONObject("requestStatus");
        return "Client has returned an error: " + requestStatus.getString("status") + ", " + requestStatus.getString("message") + "[" + requestStatus.getString("error_code") + "]";
    }
}
