/**
 * Copyright 2014 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.zkWrapper;

import java.util.HashMap;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.framework.recipes.shared.SharedCount;
import org.apache.curator.framework.recipes.shared.SharedCountListener;
import org.apache.curator.framework.recipes.shared.VersionedValue;
import org.apache.log4j.Logger;


/**
 * @author Marco Scavuzzo
 *
 */
public class ZKclient {
	private String connectString;
	private String countersBasePath = "/hegira/counters";
	private String synchPath = "/hegira/isSynch";
	private CuratorFramework client;
	private HashMap<String, SharedCount> counters_map;
	private SharedCount isSynch;
	private static Logger logger = Logger.getLogger(ZKclient.class);
	
	public ZKclient(String connectString){
		this.connectString = connectString;
		client = CuratorFrameworkFactory.newClient(connectString,
				new ExponentialBackoffRetry(1000, 3));
		client.start();
		counters_map = new HashMap<String, SharedCount>();
	}
	
	public int getCurrentSeqNr(String tableName){
		SharedCount count = getSharedCount(tableName);
		int seqNr = 0;
		
		seqNr = count.getCount();
		logger.debug(Thread.currentThread().getId()+" - current counter="+seqNr);

		return seqNr;
	}
	
	/**
	 * Returns a range of maximum 100 (minimum 2) unique ids to the caller for a given table.
	 * @param tableName The name of the table.
	 * @param offset the number of ids to be assigned (less than 101)
	 * @return An array containing the first and the last ids in the assigned range.
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public int[] assignSeqNrRange(String tableName, int offset) throws Exception{	
		if(offset<2 || offset>100)
			offset = 100;
		int[] range = new int[]{0,0};
		SharedCount count = getSharedCount(tableName);
		
		VersionedValue<Integer> versionedValue = count.getVersionedValue();
		int maxSeqNr = versionedValue.getValue();
		boolean setted = count.trySetCount(versionedValue, maxSeqNr+offset);
		if(!setted){
			throw new NonAtomicOpException("The operation has not been executed. Try it again");
		}
		range[0]=maxSeqNr+1;
		range[1]=maxSeqNr+offset;
		logger.debug(Thread.currentThread().getId()+" - "+tableName
				+" range=["+range[0]+","+range[1]+"]");
		
		return range;
	}
	
	/**
	 * Returns a unique id to the caller for a given table.
	 * @param tableName The name of the table.
	 * @return The unique id
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public int assignSeqNr(String tableName) throws Exception{
		SharedCount count = getSharedCount(tableName);
		VersionedValue<Integer> versionedValue = count.getVersionedValue();
		int maxSeqNr = versionedValue.getValue();
		boolean setted = count.trySetCount(versionedValue, maxSeqNr+1);
		if(!setted){
			throw new NonAtomicOpException("The operation has not been executed. Try it again");
		}
		logger.debug(Thread.currentThread().getId()+" - "+tableName
				+" SeqNr="+(maxSeqNr+1));
		return maxSeqNr+1;
	}
	
	/**
	 * Returns the status of Hegira, i.e., if it has been configured to synchronize data.
	 * @param listener	A listener which is asynchronously notified of the synchronization status
	 * @return <b>true</b> if it is synchronizing, <b>false</b> otherwise
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	public boolean isSynchronizing(SharedCountListener listener) throws Exception{
		isSynch = new SharedCount(client, synchPath, fromBoolean(false));
		isSynch.start();
		boolean synching = toBoolean(isSynch.getCount());
		if(listener!=null)
			isSynch.addListener(listener);
		return synching;
	}
	
	private void setSync(boolean value) throws Exception{
		isSynch = new SharedCount(client, synchPath, fromBoolean(false));
		isSynch.start();
		VersionedValue<Integer> vv = isSynch.getVersionedValue();
		boolean success = isSynch.trySetCount(vv, fromBoolean(value));
		int retries=0;
		while(!success && retries<=3){
			Thread.sleep(1000);
			success = isSynch.trySetCount(vv, fromBoolean(true));
			retries++;
		}
		isSynch.close();
	}
	
	protected void synchronize() throws Exception{
		setSync(true);
	}
	
	protected void unsynchronize() throws Exception{
		setSync(false);
	}
	
	/**
	 * Returns the proper SharedCount for a given table.
	 * If it doesn't exist it creates a new one and puts it in the
	 * counters_map.
	 * @param tableName The name of the table
	 * @return The SharedCount
	 */
	private SharedCount getSharedCount(String tableName){
		SharedCount sc = counters_map.get(tableName);
		String path = countersBasePath+"/"+tableName;
		if(sc==null){
			sc = new SharedCount(client, path, 0);
			try {
				sc.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
			counters_map.put(tableName, sc);
		}
		return sc;
	}
	
	public void close(){
		CloseableUtils.closeQuietly(client);
	}
	
	public static boolean toBoolean(int value){
		boolean bool = false;
		if(value==1)
			bool = true;
		return bool;
	}
	
	private static int fromBoolean(boolean value){
		int int1 = 0;
		if(value)
			int1 = 1;
		return int1;
	}
}
