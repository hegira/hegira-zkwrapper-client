/**
 * Copyright 2014 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.zkWrapper;

import static org.junit.Assert.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.shared.SharedCountListener;
import org.apache.curator.framework.recipes.shared.SharedCountReader;
import org.apache.curator.framework.state.ConnectionState;
import org.junit.Test;

/**
 * @author Marco Scavuzzo
 *
 */
public class ZKClientTest {
	String connectionString = "localhost:2181";
	
	public void assignSeqNrRange(ZKclient zKclient,String tableName) throws Exception {
		int offset = 10;
		int prev = zKclient.getCurrentSeqNr(tableName);
		int[] range = zKclient.assignSeqNrRange(tableName,offset);
		assertEquals(prev+1, range[0]);
		assertEquals(range[0]+offset-1, range[1]);
	}
	
	@Test
	public void assignSeqNrRangeTest(){
		ZKclient zKclient=null;
		try{
			zKclient = new ZKclient(connectionString);
			assignSeqNrRange(zKclient,"prova");
			assignSeqNrRange(zKclient,"prova2");
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(zKclient!=null)	
				zKclient.close();
		}
	}
	
	@Test
	public void assignSeqNr(){
		ZKclient zKclient=null;
		try{
			zKclient = new ZKclient(connectionString);
			String tableName = "prova";
			int currentSeqNr = zKclient.getCurrentSeqNr(tableName);
			int newSeqNr = zKclient.assignSeqNr(tableName);
			assertEquals(currentSeqNr+1, newSeqNr);
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(zKclient!=null)	
				zKclient.close();
		}
	}
	
	@Test
	public void assignSeqNrParallel(){
		ZKclient zKclient = new ZKclient(connectionString);
		int currentSeqNr = zKclient.getCurrentSeqNr("prova");
		
		int nThreads = 5;
		ExecutorService service = Executors.newFixedThreadPool(nThreads);
		
		for(int i=0;i<nThreads;i++){
			service.submit(new Runnable() {
				public void run() {
					System.out.println(Thread.currentThread().getId()
							+" - requestingNr");
					
					ZKclient zKclient=null;
					int j=0;
					while(j<5 && j!=-1){
						try{
							zKclient = new ZKclient(connectionString);
							String tableName = "prova";
							int currentSeqNr = zKclient.getCurrentSeqNr(tableName);
							int newSeqNr = zKclient.assignSeqNr(tableName);
							assertEquals(currentSeqNr+1, newSeqNr);
							j=-1;
						} catch(NonAtomicOpException e){
							j++;
							try {
								System.err.println(Thread.currentThread().getId()
										+" - "+j+" NonAtomicOp. Sleeping...");
								Thread.sleep(2000*j);
							} catch (InterruptedException e1) {
								e1.printStackTrace();
								j++;
							}
						}catch (Exception e) {
							e.printStackTrace();
							j++;
						}finally{
							if(zKclient!=null)	
								zKclient.close();
						}
					}
					if(j>=5){
						fail("Too many non atomic operations!");
					}
				}
			});
		}
		
		service.shutdown();
		try {
			service.awaitTermination(5, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail("Not all threads completed within time!");
		}
		
		int newSeqNr = zKclient.getCurrentSeqNr("prova");
		assertEquals(currentSeqNr+nThreads, newSeqNr);
		zKclient.close();
	}
	
	@Test
	public void testSynch(){
		ZKclient zKclient = new ZKclient(connectionString);
		try {
			zKclient.unsynchronize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		zKclient.close();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ExecutorService service = Executors.newFixedThreadPool(1);
		service.submit(new Runnable() {
			
			public void run() {
				ZKclient zKclient = new ZKclient(connectionString);
				SharedCountListener listener = new SharedCountListener() {
					
					public void stateChanged(CuratorFramework arg0, ConnectionState arg1) {
						System.out.println("State changed to: "+arg1.name());
						
					}
					
					public void countHasChanged(SharedCountReader sharedCount, int newCount)
							throws Exception {
						System.out.println("Value changed to: "+ZKclient.toBoolean(newCount));
					}
				};
				
				try {
					boolean synchronizing = zKclient.isSynchronizing(listener);
					System.out.println("Is Synchronizing? "+synchronizing);
					fail("");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		ZKclient zKclient2 = new ZKclient(connectionString);
		try {
			zKclient2.synchronize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		zKclient2.close();
	}

}
